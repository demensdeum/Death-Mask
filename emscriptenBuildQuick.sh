rm -rf data
cp -R DeathMask/data data
cd data
rm *.ogg
rm *.mp3
rm *.bmp
rm *.blend
rm *.gltf
rm *.glb
rm *.obj
cd ..
python convertTextures.py
source ../emsdk/emsdk_env.sh
EMSCRIPTEN="$EMSDK/emscripten/1.38.31/"
echo $EMSCRIPTEN
cmake -DEMSCRIPTEN=1 -DCMAKE_BUILD_TYPE=MinSizeRel -DCMAKE_TOOLCHAIN_FILE=$EMSCRIPTEN/cmake/Modules/Platform/Emscripten.cmake && make
rm -rf data
