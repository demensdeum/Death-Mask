rm -rf CMakeFiles
rm CMakeCache.txt
rm cmake_install.cmake
rm Makefile
./emscriptenBuildQuick.sh
rm -rf /var/www/html/wild
mkdir /var/www/html/wild
mkdir /var/www/html/wild/data
cp DeathMask/DeathMask.data /var/www/html/wild
cp DeathMask/DeathMask.wasm /var/www/html/wild
cp DeathMask/DeathMask.js /var/www/html/wild
cp DeathMask/indexFullscreen.html /var/www/html/wild/index.html
cp DeathMask/data/*.mp3 /var/www/html/wild/data
cp DeathMask/data/*.ogg /var/www/html/wild/data

firefox localhost/wild

rm -rf build/web
mkdir -p build/web

cp -r /var/www/html/wild ~/Sources/Death-Mask/build/web
