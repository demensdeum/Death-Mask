#include <iostream>
#include <memory>

#include <DeathMask/src/Controllers/GameController/DMGameController.h>
#include <DeathMask/src/Const/DMConstStates.h>

using namespace std;
using namespace DeathMaskGame;

#ifdef __EMSCRIPTEN__
#define DEATH_MASK_GAME_IN_GAME_TEST 0
#else
#define DEATH_MASK_GAME_IN_GAME_TEST 1
#endif

int main()
{
    cout << "Death Mask - Cyber Fantasy Adventure in Endless Techno-Maze\nPrepare to die!\n" << endl;

    try
    {

        auto controller = make_shared<DMGameController>();
#if DEATH_MASK_GAME_IN_GAME_TEST
        controller->startGameFromState(InGame);
#else
        controller->startGameFromState(CompanyLogo);
#endif

    }
    catch (const std::exception &exc)
    {
        cout << exc.what() << endl;
    }
    catch (...)
    {
        std::cerr << "Caught unknown exception." << std::endl;
    }

}