#include "DMInGameController.h"

#include <iostream>
#include <FlameSteelCore/FSCUtils.h>
#include <FlameSteelCore/Message.h>
#include <DeathMask/src/Utils/DMUtils.h>
#include <DeathMask/src/Data/GameObjectsGenerator.h>
#include <DeathMask/src/Const/DMConstClassIdentifiers.h>
#include <FlameSteelEngineGameToolkit/Utils/FSEGTUtils.h>
#include <FlameSteelEngineGameToolkit/Const/FSEGTConst.h>
#include <FlameSteelEngineGameToolkitAlgorithms/Const/Const.h>
#include <FlameSteelEngineGameToolkit/Data/Components/FSEGTFactory.h>
#include <FlameSteelEngineGameToolkit/IO/AudioPlayer/FSEGTAudioPlayer.h>
#include <DeathMask/src/Data/Components/Controls/ZombieControls/ZombieControls.h>
#include <FlameSteelEngineGameToolkitAlgorithms/Algorithms/MapGenerator/MapGenerator.h>
#include <FlameSteelEngineGameToolkitAlgorithms/Algorithms/MapGenerator/FSEGTAMapGeneratorParams.h>
#include <FlameSteelEngineGameToolkit/Controllers/FreeCameraController/FSEGTFreeCameraController.h>
#include <FlameSteelEngineGameToolkitAlgorithms/Algorithms/MazeObjectGenerator/FSGTAMazeObjectGenerator.h>
#include <FlameSteelEngineGameToolkit/Data/Components/Touch/FSEGTTouch.h>
#include <FlameSteelEngineGameToolkitFSGL/Data/FSGTIOFSGLSystemFactory.h>
#include "IntersectionController.h"

#include "GameplayRulesController.h"

#if __EMSCRIPTEN__
#define DEATHMASK_MAP_GENERATOR_PLAYGROUND_ENABLED 0
#else
#define DEATHMASK_MAP_GENERATOR_PLAYGROUND_ENABLED 1
#endif

using namespace FlameSteelCore::Utils;
using namespace DeathMaskGame;
using namespace DeathMaskGame::Difficulty;
using namespace FlameSteelEngine::GameToolkit::Algorithms;

DMInGameController::DMInGameController()
{

    screenMessage = make_shared<string>("Screen Message");

};

void DMInGameController::generateMap(bool shouldShowLoader)
{

    objectsMap = make_shared<ObjectsMap>();

    auto serializedCardModel = FSGTAMazeObjectGenerator::generatePlane(4, 2.28, make_shared<string>("com.demensdeum.deathmaskgame.loading.screen.png"));

    auto loadingObject = FSEGTFactory::makeOnSceneObject(
                             make_shared<string>("Demensdeum Logo"),
                             make_shared<string>("Demensdeum Logo"),
                             shared_ptr<string>(),
                             shared_ptr<string>(),
                             serializedCardModel->serializedModelString(),
                             -2, -1.12, -2,
                             1, 1, 1,
                             0, 0, 0,
                             0);

    auto flag2D = FSEGTFactory::makeBooleanComponent();
    flag2D->setInstanceIdentifier(make_shared<string>(FSEGTConstComponentsFlag2D));
    flag2D->setClassIdentifier(make_shared<string>(FSEGTConstComponentsFlag2D));

    loadingObject->addComponent(flag2D);

    camera = FSEGTFactory::makeOnSceneObject(
                 make_shared<string>("camera"),
                 make_shared<string>("camera"),
                 shared_ptr<string>(),
                 shared_ptr<string>(),
                 shared_ptr<string>(),
                 0, 0, 0,
                 1, 1, 1,
                 0, 0, 0,
                 0);

    objectsContext->addObject(loadingObject);
    objectsContext->addObject(camera);

    renderer->render(gameData);

    if (shouldShowLoader == true)
    {
        shouldGenerateMapOnNextStep = true;
        return;
    }

    auto mapGeneratorParams = make_shared<FSEGTAMapGeneratorParams>();

    shared_ptr<Object> freeTile = std::make_shared<Object>();
    shared_ptr<Object> solidTile = std::make_shared<Object>();

#if DEATHMASK_MAP_GENERATOR_PLAYGROUND_ENABLED
    mapGeneratorParams->enemiesCount = 1;
    mapGeneratorParams->itemsCount = 6;

    mapGeneratorParams->mapWidth = 16;
    mapGeneratorParams->mapHeight = 16;
#else
    mapGeneratorParams->enemiesCount = 10 + RandomInt(10);
    mapGeneratorParams->itemsCount = 10 + RandomInt(30);

    mapGeneratorParams->mapWidth = 32 + RandomInt(32);
    mapGeneratorParams->mapHeight = 32 + RandomInt(32);
#endif

    mapGeneratorParams->freeTileIndex = 0;
    mapGeneratorParams->solidTileIndex = 1;

    mapGeneratorParams->tiles.push_back(freeTile);
    mapGeneratorParams->tiles.push_back(solidTile);

    mapGeneratorParams->maxIterations = 255;
    mapGeneratorParams->minCursorSize = 2;
    mapGeneratorParams->maxCursorSize = 1 + RandomInt(6);
    mapGeneratorParams->maxLineLength = 6 + RandomInt(6);

    auto objects = make_shared<Objects>();
    auto objectsGenerator = make_shared<GameObjectsGenerator>();

    auto itemsCount = mapGeneratorParams->itemsCount;

    for (auto i = 0; i < itemsCount; i++)
    {
#if DEATHMASK_MAP_GENERATOR_PLAYGROUND_ENABLED
        //auto item = objectsGenerator->makeAssaultRifle(ioSystem->materialLibrary);
        //auto item = objectsGenerator->makeShotgun(ioSystem->materialLibrary);
        //auto item = objectsGenerator->makeGSD(ioSystem->materialLibrary);
        auto item = objectsGenerator->generateRandomItem(easy, ioSystem->materialLibrary);
#else
        auto item = objectsGenerator->generateRandomItem(easy, ioSystem->materialLibrary);
#endif
        objects->addObject(item);
    }

    mapGeneratorParams->objects = objects;

    enemies = make_shared<Objects>();
    auto mapParametersEnemies = make_shared<Objects>();
    auto enemiesCount = mapGeneratorParams->enemiesCount;

    for (auto i = 0; i < enemiesCount; i ++)
    {
        auto enemy = objectsGenerator->generateEnemy(hard, ioSystem->materialLibrary);

        enemies->addObject(enemy);
        mapParametersEnemies->addObject(enemy);

        auto controls = DMUtils::getObjectZombieControls(enemy);
        controls->enableLookAt(mainCharacter);
    }

    mapGeneratorParams->enemies = mapParametersEnemies;

    auto gameMap = MapGenerator::generate(mapGeneratorParams, objectsContext);

    gameData->gameMap = gameMap;

    auto startPoint = objectsContext->objectWithInstanceIdentifier(make_shared<string>(ConstMapEntityStartPoint));

    if (startPoint.get() == nullptr)
    {
        throw logic_error("DMInGameController::beforeStart - no start point on map, can't place camera.");
    }

    auto text = make_shared<FSEGTText>(make_shared<string>("Start Point Entity"));
    text->setClassIdentifier(make_shared<string>(DMConstClassIdentifierLabel.c_str()));
    startPoint->addComponent(text);

    auto startPointPosition = FSEGTUtils::getObjectPosition(startPoint);

    if (startPointPosition.get() == nullptr)
    {
        throw logic_error("DMInGameController::beforeStart - no start point position, wrong object or incorrect arrangement.");
    }

    FSEGTUtils::getObjectPosition(camera)->x = startPointPosition->x;
    FSEGTUtils::getObjectPosition(camera)->y = startPointPosition->y;
    FSEGTUtils::getObjectPosition(camera)->z = startPointPosition->z;

    auto texturePath = make_shared<string>("data/com.demensdeum.testenvironment.maze.textures.red.png");
    auto city = FSGTAMazeObjectGenerator::generate(gameMap, texturePath);

    text = make_shared<FSEGTText>(make_shared<string>("City Model"));
    text->setClassIdentifier(make_shared<string>(DMConstClassIdentifierLabel.c_str()));
    city->addComponent(text);

    text = make_shared<FSEGTText>(make_shared<string>("Camera"));
    text->setClassIdentifier(make_shared<string>(DMConstClassIdentifierLabel.c_str()));
    camera->addComponent(text);

    text = make_shared<FSEGTText>(make_shared<string>("Main Character"));
    text->setClassIdentifier(make_shared<string>(DMConstClassIdentifierLabel.c_str()));
    mainCharacter->addComponent(text);

    playerObjectControls = make_shared<DMPlayerObjectControls>(mainCharacter, ioSystem->inputController, gameMap, stickController);

    auto mainCharacterPosition =  FSEGTUtils::getObjectPosition(mainCharacter);
    mainCharacterPosition->x = startPointPosition->x + 0.5;
    mainCharacterPosition->y = startPointPosition->y;
    mainCharacterPosition->z = startPointPosition->z + 0.5;

    if (gamePlayPropertiesWasAdded == false)
    {
        auto gameplayProperties = make_shared<DMGameplayProperties>();
        mainCharacter->addComponent(gameplayProperties);

        gameplayProperties->name = make_shared<string>("Razorback");
        gameplayProperties->health = 100;
        gameplayProperties->healthMax = 100;
        gameplayProperties->synergy = 100;
        gameplayProperties->synergyMax = 100;

        gameplayProperties->creatureType = CreatureType::living;

        auto initialWeapon = GameObjectsGenerator().makeShotgun(ioSystem->materialLibrary);

        auto overlapObjectFlag = FSEGTFactory::makeBooleanComponent();
        overlapObjectFlag->setInstanceIdentifier(make_shared<string>("Overlap Object"));
        overlapObjectFlag->setClassIdentifier(make_shared<string>("Overlap Object"));

        initialWeapon->addComponent(overlapObjectFlag);

        gameplayProperties->weapon = initialWeapon;

        objectsContext->addObject(initialWeapon);

        gamePlayPropertiesWasAdded  = true;
    }
    else
    {
        auto mainCharacterGameplayProperties = DMUtils::getObjectGameplayProperties(mainCharacter);
        auto weapon = mainCharacterGameplayProperties->weapon;
        objectsContext->addObject(weapon);
    }

    exitPoint =  objectsContext->objectWithInstanceIdentifier(make_shared<string>(ConstMapEntityEndPoint));

    portalController = make_shared<PortalController>(exitPoint, objectsContext, mainCharacter);

    if (exitPoint.get() == nullptr)
    {
        throw logic_error("DMInGameController::beforeStart - no exit point.");
    }

    text = make_shared<FSEGTText>(make_shared<string>("End Point Entity"));
    text->setClassIdentifier(make_shared<string>(DMConstClassIdentifierLabel.c_str()));
    exitPoint->addComponent(text);

    objectsContext->addObject(camera);
    objectsContext->addObject(city);
    objectsContext->addObject(mainCharacter);

    auto mainCharacterGameplayProperties = DMUtils::getObjectGameplayProperties(mainCharacter);

    if (ioSystem->params == nullptr)
    {
        throwRuntimeException("ioSystem->params == nullptr");
    }

    if (ioSystem->params->title == nullptr)
    {
        throwRuntimeException("ioSystem->params->title == nullptr");
    }

    inGameUserInterfaceController = make_shared<DeathMaskGame::InGameUserInterfaceController>(ioSystem->params->title,
                                    camera,
                                    mainCharacterGameplayProperties,
                                    shared_from_this());
    auto gameRulesObjects = make_shared<Objects>();

    for (auto i = 0; i < enemies->size(); i++)
    {
        auto enemy = enemies->objectAtIndex(i);
        gameRulesObjects->addObject(enemy);
    }

    gameRulesObjects->addObject(mainCharacter);

    explosionsController = make_shared<ExplosionsController>(objectsContext);
    gameplayRulesController = make_shared<GameplayRulesController>(gameRulesObjects, mainCharacter, shared_from_this(), objectsContext);

    uiObject = inGameUserInterfaceController->uiObject;

    objectsContext->addObject(inGameUserInterfaceController->uiObject);

    auto skybox = GameObjectsGenerator::generateSkybox();
    objectsContext->addObject(skybox);

    skyboxController = make_shared<SkyboxController>(mainCharacter, skybox, shared_from_this());

    cameraController = make_shared<CameraController>(mainCharacter, camera, objectsContext);
    weaponController = make_shared<WeaponController>(mainCharacter, camera, objectsContext);

}

void DMInGameController::skyboxControllerDidUpdateSkybox(shared_ptr<SkyboxController> skyboxController, shared_ptr<Object> skybox)
{

    objectsContext->updateObject(skybox);

}

void DMInGameController::showMessage(shared_ptr<string> message)
{

    screenMessage = message;
    screenMessageTimer = 240;

}

void DMInGameController::gameplayRulesControllerMainCharacterDidDie(shared_ptr<GameplayRulesController> gameplayRulesController, shared_ptr<Object> mainCharacter)
{

    auto gameOverMessage = make_shared<Message>(make_shared<string>("About gameplay process"), make_shared<string>("We need to switch level, because main character died"));
    messages.push_back(gameOverMessage);

}

void DMInGameController::gameplayRulesControllerMainCharacterDidFoundDeathMask(shared_ptr<GameplayRulesController> gameplayRulesController)
{

    auto gameFinalMessage = make_shared<Message>(make_shared<string>("About gameplay process"), make_shared<string>("We need to finish game, because player found death-mask"));
    messages.push_back(gameFinalMessage);

}

void DMInGameController::shooterObjectHitObject(shared_ptr<Object> shooterObject, shared_ptr<Object> hitObject)
{
    if (shooterObject == mainCharacter)
    {
        if (hitObject->getClassIdentifier()->compare(DMConstClassIdentifierEnemy) == 0)
        {
            auto hitObjectPosition = FSEGTUtils::getObjectPosition(hitObject);
            explosionsController->startExplosionAt(hitObjectPosition, mainCharacter);

            removeObject(hitObject);

            ioSystem->audioPlayer->play(make_shared<string>("data/com.demensdeum.deathmaskgame.explosion.audio.json"));

            showMessage(localizedString(make_shared<string>("Enemy killed")));
        }
    }
    else if (shooterObject->getClassIdentifier()->compare(DMConstClassIdentifierEnemy) == 0)
    {

        if (hitObject == mainCharacter)
        {
            showMessage(localizedString(make_shared<string>("Hitted by object")));

            auto enemyGameplayProperties = DMUtils::getObjectGameplayProperties(shooterObject);

            auto weapon = enemyGameplayProperties->weapon;

            if (weapon.get() == nullptr)
            {
                throw logic_error("Enemy shoots without weapon, nonsense");
            }

            auto objectItemProperties = DMUtils::getObjectItemProperties(weapon);
            auto itemEffect = objectItemProperties->getRangeRandomEffect();

            auto gameplayProperties = DMUtils::getObjectGameplayProperties(mainCharacter);
            gameplayProperties->takeDamage(itemEffect);

            ioSystem->audioPlayer->play(make_shared<string>("data/com.demensdeum.deathmaskgame.combatdrone.shot.audio.json"));

        }
    }
}

void DMInGameController::useItemAtXY(shared_ptr<Objects> objects)
{

    //cout << "Trying to use objects" << endl;

    UseItemResultType useItemResultType = cant;

    for (auto i = 0; i < objects->size(); i++)
    {
        auto object = objects->objectAtIndex(i);
        useItemResultType = gameplayRulesController->objectTryingToUseItem(mainCharacter, object);
        switch (useItemResultType)
        {

        case cant:
            break;

        case picked:
        {
            ioSystem->audioPlayer->play(make_shared<string>("data/com.demensdeum.deathmask.turum.audio.json"));
            showMessage(localizedString(make_shared<string>("Item picked")));

            auto mainCharacterGameplayProperties = DMUtils::getObjectGameplayProperties(mainCharacter);
            auto weapon = mainCharacterGameplayProperties->weapon;

            if (object != weapon)
            {
                removeObject(object);
            }
            return;
        }
        break;

        case questItemNeeded:
            ioSystem->audioPlayer->play(make_shared<string>("data/com.demensdeum.deathmask.turum.audio.json"));
            showMessage(localizedString(make_shared<string>("Quest item needed")));
            return;
        }
    }
}

void DMInGameController::removeObject(shared_ptr<Object> object)
{
    objectsContext->removeObject(object);
    gameplayRulesController->removeObject(object);

    auto uuid = object->uuid;

    if (enemies->objectWithUUID(uuid).get() != nullptr)
    {
        enemies->removeObject(object);
    }
}

void DMInGameController::beforeStart()
{

    stickController = make_shared<StickController>(ioSystem->inputController);

    mainCharacter = FSEGTFactory::makeOnSceneObject(
                        make_shared<string>("main character"),
                        make_shared<string>("main character"),
                        shared_ptr<string>(),
                        shared_ptr<string>(),
                        shared_ptr<string>(),
                        0, 0, 0,
                        1, 1, 1,
                        0, 0, 0,
                        0);

    objectsContext->subscribe(shared_from_this());
    generateMap(true);

}

shared_ptr<string> DMInGameController::messageForInGameUserInterfaceController(shared_ptr<InGameUserInterfaceController> inGameUserInterfaceController)
{

    if (screenMessageTimer > 0)
    {
        return screenMessage;
    }

    return make_shared<string>("Controls: Move - Arrows, WSAD and E to use");
}

shared_ptr<Objects> DMInGameController::objectsForInGameUserInterfaceController(shared_ptr<InGameUserInterfaceController> inGameUserInterfaceController)
{

    auto mainCharacterPosition = FSEGTUtils::getObjectPosition(mainCharacter);

    if (mainCharacterPosition.get() == nullptr)
    {
        throw logic_error("Cannot get objects for inGameUserInterface because main character position is null");
    }

    auto objects = objectsMap->objectsAtXY(mainCharacterPosition->x,mainCharacterPosition->z);

    return objects;

}

void DMInGameController::objectsContextObjectRemoved(shared_ptr<FSEGTObjectsContext> context, shared_ptr<Object> object)
{
    objectsMap->removeObject(object);
}

void DMInGameController::objectsContextObjectAdded(shared_ptr<FSEGTObjectsContext> context, shared_ptr<Object> object)
{

    objectsMap->handleObject(object);

}

void DMInGameController::objectsContextObjectUpdate(shared_ptr<FSEGTObjectsContext> context, shared_ptr<Object> object)
{

    objectsMap->handleObject(object);

}

void DMInGameController::objectsContextAllObjectsRemoved(shared_ptr<FSEGTObjectsContext> context)
{

    objectsMap->removeAllObjects();

}

void DMInGameController::step()
{

    if (shouldGenerateMapOnNextStep == true)
    {
        generateMap(false);
        shouldGenerateMapOnNextStep = false;
    }

    stickController->step();
    skyboxController->step();

    if (handleMessagesAndStopIfNeeded())
    {
        return;
    }

    frameStep();
    gameplayRulesController->step();

    if (screenMessageTimer > 0)
    {
        screenMessageTimer -= 1;
    }

    for (auto i = 0; i < enemies->size(); i++)
    {
        auto enemy = enemies->objectAtIndex(i);
        auto enemyControls = DMUtils::getObjectControls(enemy);
        if (enemy.get() == nullptr)
        {
            throw logic_error("Can't control enemy - enemy controls are null");
        }
        enemyControls->step(shared_from_this());
    }

    weaponController->step();
    explosionsController->step();
    portalController->step();
}

void DMInGameController::frameStep()
{

    auto inputController = ioSystem->inputController;

    inputController->pollKey();

    playerObjectControls->step(shared_from_this());
    inGameUserInterfaceController->step();
    objectsContext->updateObject(uiObject);

    renderer->render(gameData);

    if (inputController->isExitKeyPressed())
    {

#ifndef __EMSCRIPTEN__
        //cout << "Bye-Bye!" << endl;
        ioSystem->stop();
        exit(0);
#endif

    }
    else if (inputController->isShootKeyPressed())
    {
        if (shootKeyLocked == false)
        {
            shootKeyLocked = true;
            objectDidShoot(mainCharacter);

            auto animationComponent = FSEGTFactory::makeBooleanComponent();
            animationComponent->setInstanceIdentifier(make_shared<string>("Fire"));
            animationComponent->setClassIdentifier(make_shared<string>("Should Play Animation"));

            auto mainCharacterGameplayProperties = DMUtils::getObjectGameplayProperties(mainCharacter);
            auto weapon = mainCharacterGameplayProperties->weapon;

            if (weapon.get() != nullptr)
            {
                weapon->addComponent(animationComponent);
                objectsContext->updateObject(mainCharacter);
            }
        }
    }

    if (inputController->isUseKeyPressed() == false)
    {
        useItemLocked = false;
    }

    if (!inputController->isShootKeyPressed())
    {
        shootKeyLocked = false;
    }

    if (freeCameraController.get() != nullptr)
    {
        freeCameraController->step();
    }
    else
    {
        cameraController->step();
    }

    {
        auto mainCharacterPosition = FSEGTUtils::getObjectPosition(mainCharacter);
        auto exitPosition = FSEGTUtils::getObjectPosition(exitPoint);

        if (int(mainCharacterPosition->x) == int(exitPosition->x) && int(mainCharacterPosition->y) == int(exitPosition->y) && int(mainCharacterPosition->z) ==int(exitPosition->z))
        {

            auto switchLevelMessage = make_shared<Message>(make_shared<string>("About gameplay process"), make_shared<string>("We need to switch level, because player step inside exit point."));
            messages.push_back(switchLevelMessage);

        }
    }

    {
        auto mainCharacterPosition = FSEGTUtils::getObjectPosition(mainCharacter);

        if (mainCharacterPosition.get() == nullptr)
        {
            throwRuntimeException(string("Cannot use objects for because main character position is null"));
        }

        auto objectsToUse = objectsMap->objectsAtXY(mainCharacterPosition->x,mainCharacterPosition->z);

        if (objectsToUse.get() == nullptr)
        {
            throw logic_error("Can't use empty objects");
        }

        if (useItemLocked == false)
        {

            useItemLocked = true;

            useItemAtXY(objectsToUse);

        }
    }
}

void DMInGameController::objectDidShoot(shared_ptr<Object> shooterObject)
{

    cout << "object did shoot" << endl;

    if (shooterObject->getClassIdentifier()->compare(DMConstClassIdentifierEnemy) == 0)
    {



    }
    else
    {
        ioSystem->audioPlayer->play(make_shared<string>("data/com.demensdeum.deathmaskgame.shotgun.shot.audio.json"));

        auto mainCharacterGameplayProperties = DMUtils::getObjectGameplayProperties(mainCharacter);
        mainCharacterGameplayProperties->synergy -= 4;

    }

    bool invertZ = shooterObject != mainCharacter;

    auto hittedObjects = IntersectionController::rayFromObjectIntersectsObjects(shooterObject, objectsMap, gameData->gameMap, invertZ);

    for (auto i = 0; i < hittedObjects->size(); i++)
    {

        auto hitObject = hittedObjects->objectAtIndex(i);

        shooterObjectHitObject(shooterObject, hitObject);
    }
}

bool DMInGameController::handleMessagesAndStopIfNeeded()
{

    if (messages.size() > 0)
    {
        auto message = messages.back();
        messages.pop_back();

        auto text = message->getText();

        if (text->compare("We need to switch level, because main character died") == 0)
        {
            notifyListenerAboutControllerDidFinish(this, make_shared<string>("Game Over"));
            return true;
        }
        else if (text->compare("We need to finish game, because player found death-mask") == 0)
        {
            notifyListenerAboutControllerDidFinish(this, make_shared<string>("Game Final"));
            return true;
        }
        else if (text->compare("We need to switch level, because player step inside exit point.") == 0)
        {
            generateMap(true);
            return true;
        }

        return true;
    }

    return false;
}

void DMInGameController::objectsControlsDelegateObjectDidUpdate(shared_ptr<Object> object)
{

    objectsContext->updateObject(object);
    objectsMap->handleObject(object);

}

void DMInGameController::freeCameraControllerDidUpdateCamera(shared_ptr<FSEGTFreeCameraController> freeCameraController, shared_ptr<Object> camera)
{

    objectsContext->updateObject(camera);
    objectsMap->handleObject(camera);

}

bool DMInGameController::objectsControlsIsObjectCanMoveToPosition(shared_ptr<DMObjectControls> objectControls, shared_ptr<Object> object, shared_ptr<FSEGTVector> position)
{

    auto gameMap = gameData->gameMap;
    if (gameMap.get() == nullptr)
    {
        throwRuntimeException(string("Game map is null"));
    }

    auto tileX = int(position->x);
    auto tileY = int(position->z);

    auto tile = gameMap->getTileIndexAtXY(tileX, tileY);

    return tile == 0;
}
