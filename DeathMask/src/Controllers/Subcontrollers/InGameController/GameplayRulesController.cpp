#include <ctime>
#include <FlameSteelCore/FSCUtils.h>
#include "GameplayRulesController.h"
#include <DeathMask/src/Utils/DMUtils.h>
#include <iostream>
#include <DeathMask/src/Const/DMConstClassIdentifiers.h>
#include <FlameSteelEngineGameToolkit/Data/Components/FSEGTFactory.h>
#include <FlameSteelEngineGameToolkit/Const/FSEGTConst.h>
#include <chrono>

using namespace FlameSteelCore::Utils;
using namespace DeathMaskGame;
using namespace std::chrono;

GameplayRulesController::GameplayRulesController(shared_ptr<Objects> objects, shared_ptr<Object> mainCharacter, shared_ptr<GameplayRulesControllerDelegate> delegate, shared_ptr<FSEGTObjectsContext> objectsContext)
{
    this->mainCharacter = mainCharacter;
    this->delegate = delegate;
    this->objectsContext = objectsContext;

    if (objects.get() == nullptr)
    {
        throw logic_error("Can't initialize Gameplay Rules Controller with null objects pointer");
    }

    previousSynergyTimer = 0;

    this->objects = objects;
}

void GameplayRulesController::step()
{
    auto synergyTimer = std::time(nullptr);
    auto synergyTimerTimeout = 10;
    auto synergyDecrementFactor = 1;
    auto shouldDecrementSynergy = synergyTimer  > synergyTimerTimeout + previousSynergyTimer;

    auto copiedObjects = objects->copy();

    for (auto i = 0; i < copiedObjects->size(); i++)
    {
        auto object = copiedObjects->objectAtIndex(i);
        auto gameplayProperties = DMUtils::getObjectGameplayProperties(object);

        if (gameplayProperties->creatureType == CreatureType::living && shouldDecrementSynergy)
        {
            gameplayProperties->synergy -= synergyDecrementFactor;
        }
    }

    if (shouldDecrementSynergy)
    {
        previousSynergyTimer = synergyTimer;
    }

    if (mainCharacter.get() == nullptr)
    {
        throw logic_error("Can't handle main character in gameplay rules controller, because it's null");
    }
    else
    {
        auto gameplayProperties = DMUtils::getObjectGameplayProperties(mainCharacter);
        if (gameplayProperties->isDead())
        {
            delegate->gameplayRulesControllerMainCharacterDidDie(shared_from_this(), mainCharacter);
        }
    }

    auto mainCharacterGameplayProperties = DMUtils::getObjectGameplayProperties(mainCharacter);

    if (mainCharacterGameplayProperties->bleedingCounter > 0)
    {

        auto now = system_clock::now();
        auto nowSeconds = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();

        if (nowSeconds % 2 == 0 && nowSeconds != previousBleedingTime)
        {
            mainCharacterGameplayProperties->bleedingCounter -= 1;
            mainCharacterGameplayProperties->health -= RandomInt(6);
            mainCharacterGameplayProperties->synergy -= 1;
            previousBleedingTime = nowSeconds;
        }

    }

}

void GameplayRulesController::removeObject(shared_ptr<Object> object)
{

    objects->removeObject(object);

}

UseItemResultType GameplayRulesController::objectTryingToUseItem(shared_ptr<Object> object, shared_ptr<Object> item)
{
    auto gameplayProperties = DMUtils::getObjectGameplayProperties(object);

    UseItemResultType result = cant;

    if (item->containsComponentWithIdentifier(make_shared<string>(DMConstClassIdentifierItemProperties)))
    {
        auto objectItemProperties = DMUtils::getObjectItemProperties(item);
        auto itemEffect = objectItemProperties->getRangeRandomEffect();

        if (objectItemProperties->lockedByQuestItem == true && gameplayProperties->questItem.get() == nullptr)
        {
            //cout << "Can't pickup item - quest item required to unlock" << endl;
            return questItemNeeded;
        }

        switch (objectItemProperties->type)
        {

        case synergyItem:

            gameplayProperties->addSynergy(itemEffect);
            result = picked;
            break;

        case supply:
            gameplayProperties->addHealth(itemEffect);
            result = picked;
            break;

        case questItem:
            gameplayProperties->questItem = item;
            result = picked;
            break;

        case weapon:
            if (gameplayProperties->weapon != item)
            {

                gameplayProperties->weapon->removeComponent(make_shared<string>("Overlap Object"));

                objectsContext->updateObject(gameplayProperties->weapon);

                auto oldWeapon = gameplayProperties->weapon;
                objectsContext->removeObject(oldWeapon);

                gameplayProperties->weapon = item;

                auto overlapObjectFlag = FSEGTFactory::makeBooleanComponent();
                overlapObjectFlag->setInstanceIdentifier(make_shared<string>("Overlap Object"));
                overlapObjectFlag->setClassIdentifier(make_shared<string>("Overlap Object"));

                item->addComponent(overlapObjectFlag);

                result = picked;
            }
            break;

        case deathMask:
            delegate->gameplayRulesControllerMainCharacterDidFoundDeathMask(shared_from_this()) ;
            break;

        case ItemType::count:

            throw logic_error("Can't handle \"count\" as objectItemProperties type in GameplayRulesController");

            break;

        }

        if (objectItemProperties->lockedByQuestItem == true && gameplayProperties->questItem.get() != nullptr)
        {
            gameplayProperties->questItem = nullptr;
        }
    }

    return result;
}
