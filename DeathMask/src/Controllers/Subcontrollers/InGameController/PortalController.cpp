#include "PortalController.h"
#include <FlameSteelEngineGameToolkit/Data/Components/FSEGTFactory.h>
#include <FlameSteelEngineGameToolkit/Utils/FSEGTUtils.h>
#include <FlameSteelCore/FSCUtils.h>
#include <SDL2/SDL_image.h>
#include <iostream>

using namespace FlameSteelCore::Utils;

namespace
{
const int kHorizontalFramesCount = 11;
const int kVerticalFramesCount = 11;
const int kMaxFrames = kHorizontalFramesCount * kVerticalFramesCount;
}

PortalController::PortalController(shared_ptr<Object> portalObject, shared_ptr<FSEGTObjectsContext> objectsContext, shared_ptr<Object> mainCharacterObject)
{

    lookAtRotator = make_shared<LookAtRotator>(portalObject, mainCharacterObject);

    this->objectsContext = objectsContext;

    if (portalObject.get() == nullptr)
    {
        throwRuntimeException("Can't initialize PortalController, portalObject - nullptr");
    }

    this->portalObject = portalObject;

    portalSurface = IMG_Load("data/com.demensdeum.deathmaskgame.portal.texture.png");
}

PortalController::~PortalController()
{
    SDL_FreeSurface(portalSurface);
}

void PortalController::step()
{

    lookAtRotator->step();

    auto surfaceMaterial = FSEGTUtils::getObjectSurfaceMaterial(portalObject);
    if (surfaceMaterial.get() == nullptr)
    {
        throwRuntimeException("Surface material is null... end");
    }

    auto xFrame = animationFrame % kHorizontalFramesCount;
    xFrame *= kFrameSize;

    auto yFrame = animationFrame / kVerticalFramesCount;
    yFrame *= kFrameSize;

    auto sourceRect = SDL_Rect();
    sourceRect.x = xFrame;
    sourceRect.y = yFrame;
    sourceRect.w = kFrameSize;
    sourceRect.h = kFrameSize;

    auto surface = surfaceMaterial->material->surface;
    SDL_FillRect(surface, NULL, SDL_MapRGBA(surface->format, 0, 0, 0, 0));
    SDL_BlitSurface(portalSurface, &sourceRect, surface, NULL);

    surfaceMaterial->material->needsUpdate = true;
    objectsContext->updateObject(portalObject);

    if (animationFrame < kMaxFrames -1)
    {
        animationFrame += 1;
    }
    else
    {
        animationFrame = 0;
    }

}

