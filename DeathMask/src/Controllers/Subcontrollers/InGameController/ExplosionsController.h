#ifndef DEATHMASKEXPLOSIONSCONTROLLER_H_
#define DEATHMASKEXPLOSIONSCONTROLLER_H_

#include <FlameSteelEngineGameToolkit/Data/Components/Vector/FSEGTVector.h>
#include <FlameSteelEngineGameToolkit/Controllers/FSEGTObjectsContext.h>
#include <SDL2/SDL.h>

namespace DeathMaskGame
{

class ExplosionsController
{

public:
    ExplosionsController(shared_ptr<FSEGTObjectsContext> objectsContext);
    ~ExplosionsController();
    void startExplosionAt(shared_ptr<FSEGTVector> position, shared_ptr<Object> mainCharacterObject);
    void step();

private:
    SDL_Surface *explosionSurface = nullptr;
    shared_ptr<FSEGTObjectsContext> objectsContext;
    shared_ptr<Objects> explosions;

};
};


#endif