#include "ExplosionsController.h"
#include <FlameSteelEngineGameToolkit/Data/Components/Integer/FSEGTIntegerComponent.h>
#include <FlameSteelEngineGameToolkit/Utils/FSEGTUtils.h>
#include <FlameSteelEngineGameToolkit/Const/FSEGTConst.h>
#include <FlameSteelEngineGameToolkit/Data/Components/FSEGTFactory.h>
#include <FlameSteelEngineGameToolkitAlgorithms/Algorithms/MazeObjectGenerator/FSGTAMazeObjectGenerator.h>
#include <DeathMask/src/Const/DMConstClassIdentifiers.h>
#include <DeathMask/src/Data/Components/Controls/LookAtRotator/LookAtRotator.h>
#include <FlameSteelCore/Object.h>
#include <FlameSteelCore/FSCUtils.h>
#include <SDL2/SDL_image.h>
#include <iostream>

using namespace std;
using namespace FlameSteelCore;
using namespace DeathMaskGame;
using namespace FlameSteelCore::Utils;

namespace
{
const int kHorizontalFramesCount = 4;
const int kVerticalFramesCount = 4;
const int kFrameSize = 128;
const int kMaxFrames = kHorizontalFramesCount * kVerticalFramesCount;
}

ExplosionsController::ExplosionsController(shared_ptr<FSEGTObjectsContext> objectsContext)
{
    explosionSurface = IMG_Load("data/com.demensdeum.deathmaskgame.explosion.texture.png");
    if (explosionSurface == NULL)
    {
        throwRuntimeException("explosionSurface = NULL");
    }
    explosions = make_shared<Objects>();
    this->objectsContext = objectsContext;
}

ExplosionsController::~ExplosionsController()
{
    SDL_FreeSurface(explosionSurface);
}

void ExplosionsController::startExplosionAt(shared_ptr<FSEGTVector> position, shared_ptr<Object> mainCharacterObject)
{

    auto texturePath = "com.demensdeum.deathmaskgame.explosion.texture.png";
    auto explosionPlaneModel = FSGTAMazeObjectGenerator::generatePlane(1, 1, make_shared<string>(texturePath));

    auto explosionPlaneObject = FSEGTFactory::makeOnSceneObject(
                                    make_shared<string>("Explosion"),
                                    make_shared<string>("Explosion"),
                                    shared_ptr<string>(),
                                    shared_ptr<string>(),
                                    explosionPlaneModel->serializedModelString(),
                                    position->x, 0, position->z,
                                    1, 1, 1,
                                    0, 0, 0,
                                    0);

    auto lookAtRotator = make_shared<LookAtRotator>(explosionPlaneObject, mainCharacterObject);
    lookAtRotator->setInstanceIdentifier(make_shared<string>("LookAtRotator"));
    lookAtRotator->setClassIdentifier(make_shared<string>("LookAtRotator"));
    explosionPlaneObject->addComponent(lookAtRotator);

    auto flag2D = FSEGTFactory::makeBooleanComponent();
    flag2D->setInstanceIdentifier(make_shared<string>(FSEGTConstComponentsFlag2D));
    flag2D->setClassIdentifier(make_shared<string>(FSEGTConstComponentsFlag2D));
    explosionPlaneObject->addComponent(flag2D);

    auto text = make_shared<FSEGTText>(make_shared<string>("Explosion"));
    text->setClassIdentifier(make_shared<string>(DMConstClassIdentifierLabel.c_str()));
    explosionPlaneObject->addComponent(text);

    auto surfaceMaterialComponent  = FSEGTFactory::makeSurfaceMaterialComponent(kFrameSize, kFrameSize);
    explosionPlaneObject->addComponent(surfaceMaterialComponent);

    auto componentAnimationFrame = make_shared<FSEGTIntegerComponent>(0);
    componentAnimationFrame->setClassIdentifier(make_shared<string>("animationFrame"));
    componentAnimationFrame->setInstanceIdentifier(make_shared<string>("animationFrame"));

    explosionPlaneObject->addComponent(componentAnimationFrame);
    objectsContext->addObject(explosionPlaneObject);
    explosions->addObject(explosionPlaneObject);

}

void ExplosionsController::step()
{
    for (auto i = 0; i < explosions->size(); i++)
    {
        auto explosion = explosions->objectAtIndex(i);
        auto lookAtRotatorComponent = explosion->getComponent(make_shared<string>("LookAtRotator"));
        auto lookAtRotator = static_pointer_cast<LookAtRotator>(lookAtRotatorComponent);
        lookAtRotator->step();

        auto surfaceMaterial = FSEGTUtils::getObjectSurfaceMaterial(explosion);
        if (surfaceMaterial.get() == nullptr)
        {
            throwRuntimeException("Surface material is null... end");
        }

        auto animationFrameComponent = explosion->getComponent(make_shared<string>("animationFrame"));
        auto componentAnimationFrame = static_pointer_cast<FSEGTIntegerComponent>(animationFrameComponent);
        if (componentAnimationFrame.get() == nullptr)
        {
            throwRuntimeException("No component animation frame");
        }

        auto value = componentAnimationFrame->value;

        if (value < kMaxFrames)
        {
            value += 1;
        }
        else
        {
            value = 0;
        }

        componentAnimationFrame->value = value;

        auto xFrame = value % kHorizontalFramesCount;
        xFrame *= kFrameSize;

        auto yFrame = value / kVerticalFramesCount;
        yFrame *= kFrameSize;

        auto x = xFrame * kFrameSize;
        auto y = yFrame * kFrameSize;

        auto sourceRect = SDL_Rect();
        sourceRect.x = xFrame;
        sourceRect.y = yFrame;
        sourceRect.w = kFrameSize;
        sourceRect.h = kFrameSize;

        auto surface = surfaceMaterial->material->surface;
        SDL_FillRect(surface, NULL, SDL_MapRGBA(surface->format, 0, 0, 0, 0));
        SDL_BlitSurface(explosionSurface, &sourceRect, surface, NULL);

        surfaceMaterial->material->needsUpdate = true;

        if (value < 16)
        {
            objectsContext->updateObject(explosion);
        }
        else
        {
            explosions->removeObject(explosion);
            objectsContext->removeObject(explosion);
        }
    }
}