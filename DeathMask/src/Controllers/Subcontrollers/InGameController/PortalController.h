#ifndef DEATHMASKPORTALCONTROLLER_H_
#define DEATHMASKPORTALCONTROLLER_H_

#include <SDL2/SDL.h>
#include <FlameSteelCore/Object.h>
#include <FlameSteelEngineGameToolkit/Controllers/FSEGTObjectsContext.h>
#include <DeathMask/src/Data/Components/Controls/LookAtRotator/LookAtRotator.h>

using namespace DeathMaskGame;
using namespace std;

class PortalController
{

public:
    PortalController(shared_ptr<Object> portalObject, shared_ptr<FSEGTObjectsContext> objectsContext, shared_ptr<Object> mainCharacterObject);
    ~PortalController();
    void step();

    const int kFrameSize = 256;

private:
    SDL_Surface *portalSurface = nullptr;
    shared_ptr<FSEGTObjectsContext> objectsContext;
    shared_ptr<Object> portalObject;
    int animationFrame = 0;
    shared_ptr<LookAtRotator> lookAtRotator;

};

#endif