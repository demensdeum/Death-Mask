#ifndef DEATHMASKMENUCONTROLLER_H_
#define DEATHMASKMENUCONTROLLER_H_

#include <vector>
#include <FlameSteelEngineGameToolkit/Controllers/GameController.h>
#include <FlameSteelEngineGameToolkitAlgorithms/Controllers/CursorController/CursorControllerDelegate.h>
#include <FlameSteelEngineGameToolkit/Controllers/FreeCameraController/FSEGTFreeCameraControllerDelegate.h>
#include <FlameSteelEngineGameToolkitAlgorithms/Controllers/FadeVisualEffectController/FadeVisualEffectController.h>
#include <FlameSteelEngineGameToolkitAlgorithms/Controllers/FadeVisualEffectController/FadeVisualEffectControllerDelegate.h>

namespace FlameSteelEngine
{
namespace GameToolkit
{
class FloatComponent;
};
};

using namespace FlameSteelEngine::GameToolkit::Algorithms;
using namespace FlameSteelEngine::GameToolkit::Controllers;

class FSEGTFreeCameraController;

namespace DeathMaskGame
{

class MenuController: public GameController,
    public enable_shared_from_this<MenuController>,
    public FSEGTFreeCameraControllerDelegate,
    public CursorControllerDelegate,
    public FadeVisualEffectControllerDelegate
{

public:
    virtual void beforeStart();
    virtual void step();
    virtual void beforeStop();

    virtual void freeCameraControllerDidUpdateCamera(shared_ptr<FSEGTFreeCameraController> freeCameraController, shared_ptr<Object> camera);
    virtual void cursorControllerDidUpdateCursor(shared_ptr<CursorController> cursorController, shared_ptr<Object> cursor);

    virtual void fadeVisualEffectDidFinishAnimation(shared_ptr<FadeVisualEffectController> fadeVisualEffectController, Type animationType);
    virtual void fadeVisualEffectDidUpdateObject(shared_ptr<FadeVisualEffectController> fadeVisualEffectController, shared_ptr<Object> updatedObject);

private:

    bool revertAnimationStarted = false;
    bool shouldExit = false;
    bool cameraUpdated = false;
    shared_ptr<FadeVisualEffectController> fadeVisualEffectController;
    shared_ptr<CursorController> cursorController;
    shared_ptr<Object> cursor;
    shared_ptr<Object> camera;
    shared_ptr<Object> gameLogo;
    shared_ptr<Object> sky;

    vector< shared_ptr<Object> > cityClusters;

    shared_ptr<FSEGTFreeCameraController> freeCameraController;
    vector<shared_ptr<Object> > skyscrapers;
    int cooldownTimer = 0;

    shared_ptr<Object> addCityClusterTo(float x, float y, float z);


};

};

#endif
