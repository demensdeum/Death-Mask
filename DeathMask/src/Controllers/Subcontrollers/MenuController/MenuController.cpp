#include "MenuController.h"

#include <FlameSteelEngineGameToolkitFSGL/Data/FSGTIOFSGLSystemFactory.h>
#include <FlameSteelEngineGameToolkit/Const/FSEGTConst.h>
#include <FlameSteelEngineGameToolkitAlgorithms/Algorithms/MazeObjectGenerator/FSGTAMazeObjectGenerator.h>
#include <FlameSteelEngineGameToolkit/Controllers/FreeCameraController/FSEGTFreeCameraController.h>
#include <FlameSteelEngineGameToolkitAlgorithms/Controllers/CursorController/CursorController.h>
#include <FlameSteelEngineGameToolkit/Data/Components/FSEGTFactory.h>
#include <FlameSteelEngineGameToolkit/Utils/FSEGTUtils.h>
#include <FlameSteelCore/FSCUtils.h>
#include <iostream>

using namespace std;

using namespace FlameSteelEngine::GameToolkit::Controllers;
using namespace FlameSteelEngine::GameToolkit::Algorithms;
using namespace DeathMaskGame;

void MenuController::beforeStart()
{

    fadeVisualEffectController = make_shared<FadeVisualEffectController>();
    fadeVisualEffectController->delegate = shared_from_this();

    cooldownTimer = 0;
    cameraUpdated = false;

    auto flag2D = FSEGTFactory::makeBooleanComponent();
    flag2D->setInstanceIdentifier(make_shared<string>(FSEGTConstComponentsFlag2D));
    flag2D->setClassIdentifier(make_shared<string>(FSEGTConstComponentsFlag2D));

    auto serializedGameLogoModel = FSGTAMazeObjectGenerator::generatePlane(1, 0.5625, make_shared<string>("com.demensdeum.deathmaskgame.startmenu.png"));

    gameLogo = FSEGTFactory::makeOnSceneObject(
                   make_shared<string>("Game Logo"),
                   make_shared<string>("Game Logo"),
                   shared_ptr<string>(),
                   shared_ptr<string>(),
                   serializedGameLogoModel->serializedModelString(),
                   -0.2, -0.08, -0.6,
                   2, 2, 1,
                   0, 0, 0,
                   0);

    gameLogo->addComponent(flag2D);

    objectsContext->addObject(gameLogo);

    auto skyModel = FSGTAMazeObjectGenerator::generatePlane(0.336, 0.4, make_shared<string>("com.demensdeum.deathmaskgame.sky.png"));
    sky = FSEGTFactory::makeOnSceneObject(
              make_shared<string>("Sky"),
              make_shared<string>("Sky"),
              shared_ptr<string>(),
              shared_ptr<string>(),
              skyModel->serializedModelString(),
              -0.19, 0, -0.3,
              1.4, 0.8, 1,
              0, 0, 0,
              0);

    auto flagSkybox = FSEGTFactory::makeBooleanComponent();
    flagSkybox->setInstanceIdentifier(make_shared<string>(FSEGTConstComponentsFlagSkybox));
    flagSkybox->setClassIdentifier(make_shared<string>(FSEGTConstComponentsFlagSkybox));

    sky->addComponent(flagSkybox);
    objectsContext->addObject(sky);


    auto serializedCursorModel = FSGTAMazeObjectGenerator::generatePlane(0.018, 0.025, make_shared<string>("com.demensdeum.deathmaskgame.cursor.png"));

    cursor = FSEGTFactory::makeOnSceneObject(
                 make_shared<string>("Game Cursor"),
                 make_shared<string>("Game Cursor"),
                 shared_ptr<string>(),
                 shared_ptr<string>(),
                 serializedCursorModel->serializedModelString(),
                 -0.2, -0.08, -0.4,
                 1, 1, 1,
                 0, 0, 0,
                 0);

    cursor->addComponent(flag2D);
    objectsContext->addObject(cursor);

    camera = FSEGTFactory::makeOnSceneObject(
                 make_shared<string>("camera"),
                 make_shared<string>("camera"),
                 shared_ptr<string>(),
                 shared_ptr<string>(),
                 shared_ptr<string>(),
                 0, 0.1, -0.2,
                 1, 1, 1,
                 -0.2, 0, 0,
                 0);

    freeCameraController = make_shared<FSEGTFreeCameraController>(ioSystem->inputController, camera);
    freeCameraController->delegate = shared_from_this();

    cursorController = make_shared<CursorController>(cursor, ioSystem->inputController);
    cursorController->delegate = shared_from_this();

    cityClusters.push_back(addCityClusterTo(-1.8, -0.47, -1.2));
    cityClusters.push_back(addCityClusterTo(-1.8, -0.47, -3.2));
    cityClusters.push_back(addCityClusterTo(0.12, -0.47, -1.2));
    cityClusters.push_back(addCityClusterTo(0.12, -0.47, -3.2));
    cityClusters.push_back(addCityClusterTo(2.12, -0.47, -1.2));
    cityClusters.push_back(addCityClusterTo(2.12, -0.47, -3.2));
    cityClusters.push_back(addCityClusterTo(-1.8, -0.47, -5.2));
    cityClusters.push_back(addCityClusterTo(0.12, -0.47, -5.2));
    cityClusters.push_back(addCityClusterTo(2.12, -0.47, -5.2));

    fadeVisualEffectController->addObject(gameLogo);
    fadeVisualEffectController->startFadeIn();


}

shared_ptr<Object> MenuController::addCityClusterTo(float x, float y, float z)
{

    auto modelPath = make_shared<string>("data/com.demensdeum.deathmaskgame.city.cluster.fsglmodel");

    auto cityClusterObject = FSEGTFactory::makeOnSceneObject(
                                 make_shared<string>("City Cluster"),
                                 make_shared<string>("City Cluster"),
                                 shared_ptr<string>(),
                                 modelPath,
                                 shared_ptr<string>(),
                                 x, y, z,
                                 1, 1, 1,
                                 0, 0, 0,
                                 0);

    objectsContext->addObject(cityClusterObject);
    fadeVisualEffectController->addObject(cityClusterObject);

    return cityClusterObject;
}

void MenuController::beforeStop()
{
    skyscrapers.clear();
    fadeVisualEffectController->removeAllObjects();
}

void MenuController::freeCameraControllerDidUpdateCamera(shared_ptr<FSEGTFreeCameraController> freeCameraController, shared_ptr<Object> camera)
{

    //objectsContext->updateObject(camera);

}

void MenuController::cursorControllerDidUpdateCursor(shared_ptr<CursorController> cursorController, shared_ptr<Object> cursor)
{

    objectsContext->updateObject(cursor);

}

void MenuController::step()
{



    if (cameraUpdated == false)
    {
        objectsContext->updateObject(camera);
        cameraUpdated = true;
    }

    if (cursorController.get() != nullptr)
    {
        cursorController->step();
    }

    if (freeCameraController.get() != nullptr)
    {
        //freeCameraController->step();
    }

    if (fadeVisualEffectController.get() != nullptr)
    {
        fadeVisualEffectController->step();
    }

    for (auto cityCluster : cityClusters)
    {
        objectsContext->updateObject(cityCluster);
    }

    if (shouldExit == true)
    {
        notifyListenerAboutControllerDidFinish(this);
        return;
    }

    renderer->render(gameData);

    ioSystem->inputController->pollKey();

    if (ioSystem->inputController->isExitKeyPressed())
    {
#ifndef __EMSCRIPTEN__
        ioSystem->stop();
        exit(0);
#endif
    }
    else if (cooldownTimer < 80)
    {
        cooldownTimer++;
    }
    else if (ioSystem->inputController->isShootKeyPressed())
    {
        fadeVisualEffectController->startFadeOut();
    }

    auto moveStep = 0.0001;

    auto cameraPosition = FSEGTUtils::getObjectPosition(camera);
    cameraPosition->z -= moveStep;

    //cout << cityClusterPosition->z << endl;

    if (cameraPosition->z < -2.2 && revertAnimationStarted == false)
    {
        revertAnimationStarted = true;
        fadeVisualEffectController->startFadeOut();
    }

    auto cursorPosition = FSEGTUtils::getObjectPosition(cursor);
    cursorPosition->z -= moveStep;

    objectsContext->updateObject(camera);

    FSEGTUtils::getObjectPosition(sky)->x = cameraPosition->x - 0.24;
    FSEGTUtils::getObjectPosition(sky)->y = cameraPosition->y - 0.1;
    FSEGTUtils::getObjectPosition(sky)->z = cameraPosition->z - 0.22;
    FSEGTUtils::getObjectRotation(sky)->x = FSEGTUtils::getObjectRotation(camera)->x;
    objectsContext->updateObject(sky);

    FSEGTUtils::getObjectRotation(gameLogo)->x = FSEGTUtils::getObjectRotation(camera)->x;
    FSEGTUtils::getObjectPosition(gameLogo)->x = cameraPosition->x - 1;
    FSEGTUtils::getObjectPosition(gameLogo)->y = cameraPosition->y - 0.72;
    FSEGTUtils::getObjectPosition(gameLogo)->z = cameraPosition->z - 0.8;

    objectsContext->updateObject(gameLogo);
}

void MenuController::fadeVisualEffectDidFinishAnimation(shared_ptr<FadeVisualEffectController> fadeVisualEffectController, Type animationType)
{
    cout << "did finish animation: " << animationType << endl;
    if (animationType == fadeOutType)
    {

        revertAnimationStarted = false;
        fadeVisualEffectController->startFadeIn();

        auto cityClusterPosition = FSEGTUtils::getObjectPosition(camera);
        cityClusterPosition->z = 0;

        auto cursorPosition = FSEGTUtils::getObjectPosition(cursor);
        cursorPosition->z = -0.4;

        shouldExit = true;
    }
}

void MenuController::fadeVisualEffectDidUpdateObject(shared_ptr<FadeVisualEffectController> fadeVisualEffectController, shared_ptr<Object> updatedObject)
{

}