/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   DMGameController.cpp
 * Author: demensdeum
 *
 * Created on April 16, 2017, 5:58 PM
 */

#include "DMGameController.h"

#include <ctime>
#include <iostream>

#include <FlameSteelEngineGameToolkit/IO/IOSystems/FSEGTIOSystemParams.h>
#include <FlameSteelEngineGameToolkit/IO/AudioPlayer/FSEGTAudioPlayer.h>

#include <FlameSteelCore/FSCUtils.h>

#include <DeathMask/src/Const/DMConstStates.h>
#include <DeathMask/src/Const/DMConstIOSystem.h>
#include <DeathMask/src/Controllers/Subcontrollers/CreditsController/CreditsController.h>
#include <DeathMask/src/Controllers/Subcontrollers/InGameController/DMInGameController.h>
#include <DeathMask/src/Controllers/Subcontrollers/MenuController/MenuController.h>
#include <DeathMask/src/Controllers/Subcontrollers/GameOverController/GameOverController.h>
#include <DeathMask/src/Controllers/Subcontrollers/GameFinalController/GameFinalController.h>

#include <FlameSteelEngineGameToolkitFSGL/FSEGTIOFSGLSystem.h>

#ifdef __EMSCRIPTEN__
#define DEATHMASKGAME_LOCK_SCREEN_RESOLUTION 0
#else
#define DEATHMASKGAME_LOCK_SCREEN_RESOLUTION 1
#endif
#define DEATHMASK_PLAY_MUSIC 1

using namespace FlameSteelCore::Utils;

DMGameController::DMGameController()
{

    // random seed

    srand (time(NULL));

    // IO System

    auto ioSystemParams = make_shared<FSEGTIOSystemParams>();
    ioSystemParams->title = localizedString(make_shared<string>("Death Mask v1.02"));

    ioSystem = makeIOSystem();
    ioSystem->preInitialize();
    ioSystem->fillParams(ioSystemParams);

#if DEATHMASKGAME_LOCK_SCREEN_RESOLUTION
    ioSystemParams->width = 1024;
    ioSystemParams->height = 576;
    ioSystemParams->windowed = true;
#endif

    ioSystem->initialize(ioSystemParams);

    auto companyLogoController = make_shared<CreditsController>();
    companyLogoController->logoPath = make_shared<string>("com.demensdeum.logo.png");
    setControllerForState(companyLogoController, CompanyLogo);

    auto flameSteelEngineController = make_shared<CreditsController>();
    flameSteelEngineController->logoPath = make_shared<string>("com.demensdeum.flamesteelengine.png");
    setControllerForState(flameSteelEngineController, FlameSteelEngineLogo);

    auto menuController = make_shared<MenuController>();
    setControllerForState(menuController, Menu);

    auto inGameController = make_shared<DMInGameController>();
    setControllerForState(inGameController, InGame);

    auto gameOverController = make_shared<GameOverController>();
    setControllerForState(gameOverController,GameOver);

    auto gameFinalController = make_shared<GameFinalController>();
    setControllerForState(gameFinalController, GameFinal);
}

shared_ptr<FSEGTIOSystem> DMGameController::makeIOSystem()
{
    auto ioSystem = make_shared<FSEGTIOFSGLSystem>();
    return ioSystem;
}

void DMGameController::controllerDidFinish(Controller *controller, shared_ptr<string> message)
{

    switch (state)
    {

    case CompanyLogo:
        objectsContext->removeAllObjects();
        switchToState(FlameSteelEngineLogo);
        break;

    case FlameSteelEngineLogo:

#if DEATHMASK_PLAY_MUSIC == 1
        ioSystem->audioPlayer->play(make_shared<string>("data/com.demensdeum.deathmaskgame.apnea.audio.json"));
#endif

        objectsContext->removeAllObjects();
        switchToState(Menu);
        break;

    case Menu:

#if DEATHMASK_PLAY_MUSIC == 1
        ioSystem->audioPlayer->stop(make_shared<string>("data/com.demensdeum.deathmaskgame.nephilim.audio.json"));
        ioSystem->audioPlayer->stop(make_shared<string>("data/com.demensdeum.deathmaskgame.apnea.audio.json"));
        ioSystem->audioPlayer->play(make_shared<string>("data/com.demensdeum.deathmaskgame.nephilim.audio.json"));
#endif
        ioSystem->materialLibrary->clear();
        objectsContext->removeAllObjects();
        switchToState(InGame);
        break;

    case InGame:



        if (message.get() == nullptr)
        {
            throwRuntimeException(string("INGAME CONTROLLER FINISHED WITHOUT MESSAGE, CAN'T CONTINUE"));
        }

        ioSystem->materialLibrary->clear();
        objectsContext->removeAllObjects();

        if (message->compare("Game Over") == 0)
        {
            switchToState(GameOver);
        }
        else if (message->compare("Game Final") == 0)
        {
            switchToState(GameFinal);
        }

        break;

    case GameOver:
        ioSystem->materialLibrary->clear();
        objectsContext->removeAllObjects();
        switchToState(Menu);
        break;

    case GameFinal:
        ioSystem->materialLibrary->clear();
        objectsContext->removeAllObjects();
        switchToState(Menu);
        break;

    }
}

DMGameController::DMGameController(const DMGameController& )
{
}

DMGameController::~DMGameController()
{
}
