/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   DMConst.h
 * Author: demensdeum
 *
 * Created on April 16, 2017, 6:04 PM
 */

#ifndef DMCONST_H
#define DMCONST_H

namespace DeathMaskGame
{

enum State
{

    CompanyLogo,
    FlameSteelEngineLogo,
    Menu,
    InGame,
    GameOver,
    GameFinal
};

};

#endif /* DMCONST_H */

