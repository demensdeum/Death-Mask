#ifndef DEATHMASK_DEFINED
#define DEATHMASK_DEFINED

namespace DeathMaskGame
{

namespace Difficulty
{

enum Difficulty
{

    easy,
    normal,
    hard

};

};

enum ItemType
{

    weapon,
    supply,
    questItem,
    synergyItem,
    deathMask,
    count

};

enum CreatureType
{

    unknown,
    living,
    ghost,
    undead

};

namespace WeaponType
{
enum WeaponType
{
    shotgun,
    pulseRifle,
    darkMatterInjector,
    count
};
};
};

#endif
