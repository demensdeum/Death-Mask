#include "GameObjectsGenerator.h"

#include <SDL2/SDL_image.h>
#include <DeathMask/src/Data/Components/Controls/ZombieControls/ZombieControls.h>
#include <FlameSteelEngineGameToolkit/Data/Components/FSEGTFactory.h>
#include <FlameSteelEngineGameToolkit/Data/Components/Text/FSEGTText.h>
#include <DeathMask/src/Const/DMConstClassIdentifiers.h>
#include <DeathMask/src/Data/Components/Item/ItemProperties.h>
#include <FlameSteelCore/FSCUtils.h>
#include <DeathMask/src/Data/Components/GameplayProperties/DMGameplayProperties.h>
#include <string>
#include <iostream>
#include <FlameSteelEngineGameToolkitAlgorithms/Algorithms/MazeObjectGenerator/FSGTAMazeObjectGenerator.h>
#include <DeathMask/src/Const/Const.h>
#include <FlameSteelEngineGameToolkit/Const/FSEGTConst.h>
#include <FlameSteelEngineGameToolkit/Data/Components/Text/FSEGTText.h>

using namespace FlameSteelCore::Utils;
using namespace DeathMaskGame;

GameObjectsGenerator::GameObjectsGenerator()
{
}

using namespace DeathMaskGame;
using namespace DeathMaskGame::Difficulty;

shared_ptr<Object> GameObjectsGenerator::generateRandomItem(enum Difficulty itemDifficulty, weak_ptr<MaterialLibrary> materialLibrary)
{

    auto randomItemType = RandomInt(ItemType::count);

    if (randomItemType == deathMask)
    {
        auto luckyDay = RandomInt(10) == 3;
        if (luckyDay != true)
        {
            randomItemType = synergyItem;
        }
    }

    auto item = shared_ptr<Object>();

    switch (randomItemType)
    {

    case weapon:

        //cout << "Generate weapon item" << endl;

        item = generateWeapon(itemDifficulty, materialLibrary);
        break;

    case supply:

        //cout << "Generate supply item" << endl;

        item  = generateSupplyItem(itemDifficulty);
        break;

    case questItem:

        //cout << "Generate quest item" << endl;

        item = generateQuestItem(itemDifficulty);
        break;

    case synergyItem:

        //cout << "Generate synergy item" << endl;

        item = generateSynergyItem(itemDifficulty);
        break;

    case deathMask:

        //cout << "Generate death-mask item. yay!" << endl;

        item = generateDeathMask();
        break;

    case count:
        throw logic_error("Can't generate item with incorrect type");

    }

    return item;

}

shared_ptr<Object> GameObjectsGenerator::generateDeathMask()
{

    auto firstNames = vector<string> {*localizedString(make_shared<string>("Death-Mask"))};
    auto secondNames = vector<string> {""};
    auto type = ItemType::deathMask;

    return generateObject(type, Difficulty::hard, firstNames, secondNames, false, 0, 0);

}

shared_ptr<Object> GameObjectsGenerator::generateQuestItem(enum Difficulty difficulty)
{

    auto firstNames = vector<string> {*localizedString(make_shared<string>("Red")), *localizedString(make_shared<string>("Yellow")), *localizedString(make_shared<string>("Blue"))};
    auto secondNames = vector<string> {*localizedString(make_shared<string>("Card")), *localizedString(make_shared<string>("Key"))};
    auto type = ItemType::questItem;

    return generateObject(type, difficulty, firstNames, secondNames, false, 0, 0);

}

shared_ptr<SurfaceMaterial> GameObjectsGenerator::makeSurfaceMaterialWeaponHUD(shared_ptr<string> path, SDL_Rect rect, weak_ptr<MaterialLibrary> materialLibrary)
{

    auto materialLibraryLocked = materialLibrary.lock();
    if (materialLibraryLocked.get() == nullptr)
    {
        throw logic_error("Material Library is null, can't get surface material");
    }

    auto surfaceMaterial = materialLibraryLocked->surfaceMaterialForPath(path);

    if (surfaceMaterial.get() == nullptr)
    {

        surfaceMaterial = FSEGTFactory::makeSurfaceMaterialComponent(1024, 1024);

        surfaceMaterial->setInstanceIdentifier(make_shared<string>("Weapon HUD Image"));
        surfaceMaterial->setClassIdentifier(make_shared<string>("Weapon HUD Image"));

        surfaceMaterial->debugIdentifier = "WEAPON SURFACE MATERIAL";

        materialLibraryLocked->setSurfaceMaterialForPath(surfaceMaterial, path);
    }

    return surfaceMaterial;

}

shared_ptr<Object> GameObjectsGenerator::generateSkybox()
{

    auto skyboxSerializedModel = FSGTAMazeObjectGenerator::generateSkybox(make_shared<string>("com.demensdeum.testenvironment.skybox.png"));

    auto skybox = FSEGTFactory::makeOnSceneObject(
                      make_shared<string>("dummy"),
                      make_shared<string>("dummy"),
                      shared_ptr<string>(),
                      shared_ptr<string>(),
                      skyboxSerializedModel->serializedModelString(),
                      0, 0, 0,
                      1, 1, 1,
                      0, 0, 0,
                      0);

    skybox->setClassIdentifier(make_shared<string>("skybox"));
    skybox->setInstanceIdentifier(make_shared<string>(skybox->uuid));

    auto flagSkybox = FSEGTFactory::makeBooleanComponent();
    flagSkybox->setInstanceIdentifier(make_shared<string>(FSEGTConstComponentsFlagSkybox));
    flagSkybox->setClassIdentifier(make_shared<string>(FSEGTConstComponentsFlagSkybox));

    skybox->addComponent(flagSkybox);

    auto text = make_shared<FSEGTText>(make_shared<string>("Skybox"));
    text->setClassIdentifier(make_shared<string>(DMConstClassIdentifierLabel.c_str()));
    skybox->addComponent(text);

    return skybox;
}

shared_ptr<Object> GameObjectsGenerator::makeObject(ItemType type,
        shared_ptr<string> name,
        int minimalEffect,
        int maximalEffect,
        bool lockedByQuestItem,
        shared_ptr<string> modelPath)
{

    float yPosition = 0;

    if (type == weapon)
    {
        yPosition = 0.5;
    }

    auto item = FSEGTFactory::makeOnSceneObject(
                    make_shared<string>("dummy"),
                    make_shared<string>("dummy"),
                    shared_ptr<string>(),
                    modelPath,
                    shared_ptr<string>(),
                    0, 0, 0,
                    1, 1, 1,
                    0, 0, 0,
                    0);

    item->setClassIdentifier(make_shared<string>(DMConstClassIdentifierItem));
    item->setInstanceIdentifier(make_shared<string>(item->uuid));

    auto text = make_shared<FSEGTText>(name);
    text->setClassIdentifier(make_shared<string>(DMConstClassIdentifierLabel.c_str()));
    item->addComponent(text);

    auto itemProperties = make_shared<ItemProperties>(type, minimalEffect, maximalEffect, lockedByQuestItem);
    item->addComponent(itemProperties);

    return item;
}

shared_ptr<Object> GameObjectsGenerator::generateObject(ItemType type, enum Difficulty itemDiffuclty, vector<string>firstNames, vector<string>secondNames, bool lockedByQuestItem, int minimalEffect, int maximalEffect)
{

    auto firstRandom = RandomInt(firstNames.size());
    auto secondRandom = RandomInt(secondNames.size());

    auto label = firstNames[firstRandom];
    label += " ";
    label += secondNames[secondRandom];

    auto name = make_shared<string>(label);

    auto crateModelPath = make_shared<string>("data/com.demensdeum.deathmaskgame.crate.fsglmodel");

    auto item = makeObject(type, name, minimalEffect, maximalEffect, lockedByQuestItem, crateModelPath);

    return item;
};

shared_ptr<Object> GameObjectsGenerator::generateEnemy(enum Difficulty enemyDifficulty, weak_ptr<MaterialLibrary> materialLibrary)
{

    auto path = make_shared<string>("data/com.demensdeum.deathmaskgame.combatdrone.fsglmodel");
    auto scale = 2;

    switch (enemyDifficulty)
    {
    case easy:
        path = make_shared<string>("data/com.demensdeum.deathmaskgame.combatdrone.fsglmodel");
        break;

    case normal:
        path = make_shared<string>("data/com.demensdeum.deathmaskgame.combatdrone.fsglmodel");
        break;

    case hard:
        path = make_shared<string>("data/com.demensdeum.deathmaskgame.combatdrone.fsglmodel");
        break;
    }


    auto object = FSEGTFactory::makeOnSceneObject(
                      make_shared<string>("dummy"),
                      make_shared<string>("dummy"),
                      shared_ptr<string>(),
                      path,
                      shared_ptr<string>(),
                      0, -0.5, 0,
                      scale, scale, scale,
                      0, 0, 0,
                      0);

    object->setClassIdentifier(make_shared<string>(DMConstClassIdentifierEnemy));
    object->setInstanceIdentifier(make_shared<string>(object->uuid));

    auto text = make_shared<FSEGTText>(make_shared<string>("Enemy"));
    text->setClassIdentifier(make_shared<string>(DMConstClassIdentifierLabel.c_str()));
    object->addComponent(text);

    auto gameplayProperties = make_shared<DMGameplayProperties>();
    object->addComponent(gameplayProperties);

    auto zombieControls = make_shared<ZombieControls>(object);
    object->addComponent(zombieControls);

    auto hitboxRectangle = FSEGTFactory::makeRectangle(0, 0, 1, 1);
    hitboxRectangle->setClassIdentifier(make_shared<string>("Hitbox"));
    hitboxRectangle->setInstanceIdentifier(make_shared<string>("Hitbox"));
    object->addComponent(hitboxRectangle);

    auto weapon = generateWeapon(Difficulty::easy, materialLibrary);
    gameplayProperties->weapon = weapon;

    object->debugIdentifier = "Enemy";

    return object;

};

shared_ptr<Object> GameObjectsGenerator::generateSynergyItem(enum Difficulty synergyDifficulty)
{

    auto firstNames = vector<string> {*localizedString(make_shared<string>("Synergy")), *localizedString(make_shared<string>("Synergy")) };
    auto secondNames = vector<string> {*localizedString(make_shared<string>("tank")), *localizedString(make_shared<string>("cassete")), *localizedString(make_shared<string>("tablet"))};
    auto type = ItemType::synergyItem;

    return generateObject(type,  synergyDifficulty, firstNames, secondNames, false, 10, 30);

}

shared_ptr<Object> GameObjectsGenerator::generateWeapon(enum Difficulty weaponDifficulty, weak_ptr<MaterialLibrary> materialLibrary)
{

    auto weaponType =  RandomInt(WeaponType::count);

    switch (weaponDifficulty)
    {

    case easy:

        break;

    case normal:

        break;

    case hard:

        break;

    }

    switch (weaponType)
    {

    case WeaponType::shotgun:
        return makeShotgun(materialLibrary);

    case WeaponType::pulseRifle:
        return makeAssaultRifle(materialLibrary);

    case WeaponType::darkMatterInjector:
        return makeGSD(materialLibrary);
    }

    throw logic_error("Can't generate weapon - incorrect weapon type");

};

shared_ptr<Object> GameObjectsGenerator::generateSupplyItem(enum Difficulty supplyItemDifficulty)
{

    auto firstNames = vector<string> {*localizedString(make_shared<string>("VitaCom")), *localizedString(make_shared<string>("Human Compatible Nanobots")), *localizedString(make_shared<string>("Astra-Life"))};
    auto secondNames = vector<string> {*localizedString(make_shared<string>("painkiller")), *localizedString(make_shared<string>("medkit")), *localizedString(make_shared<string>("first aid"))};
    auto type = ItemType::supply;

    return generateObject(type, supplyItemDifficulty, firstNames, secondNames, false, 10, 30);

};

shared_ptr<Object> GameObjectsGenerator::makeShotgun(weak_ptr<MaterialLibrary> materialLibrary)
{

    auto name = localizedString(make_shared<string>("Shotgun"));
    auto type = ItemType::weapon;
    auto minimalEffect = 5;
    auto maximalEffect = 10;
    auto lockedByQuestItem = false;

    auto item = makeObject(type, name, minimalEffect, maximalEffect, lockedByQuestItem, make_shared<string>("data/com.demensdeum.deathmaskgame.shotgun.fsglmodel"));

    return item;

};

shared_ptr<Object> GameObjectsGenerator::makeAssaultRifle(weak_ptr<MaterialLibrary> materialLibrary)
{

    auto name = localizedString(make_shared<string>("Assault Rifle"));
    auto type = ItemType::weapon;
    auto minimalEffect = 5;
    auto maximalEffect = 10;
    auto lockedByQuestItem = false;

    auto item = makeObject(type, name, minimalEffect, maximalEffect, lockedByQuestItem, make_shared<string>("data/com.demensdeum.deathmaskgame.assaultRifle.fsglmodel"));

    return item;
};

shared_ptr<Object> GameObjectsGenerator::makeGSD(weak_ptr<MaterialLibrary> materialLibrary)
{

    auto name = localizedString(make_shared<string>("GSD-4000"));
    auto type = ItemType::weapon;
    auto minimalEffect = 5;
    auto maximalEffect = 10;
    auto lockedByQuestItem = false;

    auto item = makeObject(type, name, minimalEffect, maximalEffect, lockedByQuestItem, make_shared<string>("data/com.demensdeum.deathmaskgame.darkMatterInjector.fsglmodel"));

    return item;

};
