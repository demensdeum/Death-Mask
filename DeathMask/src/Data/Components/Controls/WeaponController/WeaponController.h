#ifndef DEATHMASKWEAPONCONTROLLER_H_
#define DEATHMASKWEAPONCONTROLLER_H_

#include <FlameSteelEngineGameToolkit/Data/Components/Vector/FSEGTVector.h>
#include <FlameSteelEngineGameToolkit/Controllers/FSEGTObjectsContext.h>
#include <memory>

namespace DeathMaskGame
{

class WeaponController
{

public:
    WeaponController(shared_ptr<Object> object, shared_ptr<Object> camera, shared_ptr<FSEGTObjectsContext> objectsContext);
    void step();

private:
    shared_ptr<FSEGTVector> previousPosition;
    shared_ptr<Object> object;
    shared_ptr<Object> camera;
    shared_ptr<FSEGTObjectsContext> objectsContext;

    float ticks = 0;
    float amplitudeMultiplier = 0;

};

};

#endif