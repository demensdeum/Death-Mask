#include "WeaponController.h"
#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <DeathMask/src/Utils/DMUtils.h>
#include <FlameSteelEngineGameToolkit/Utils/FSEGTUtils.h>

using namespace DeathMaskGame;

WeaponController::WeaponController(shared_ptr<Object> object, shared_ptr<Object> camera, shared_ptr<FSEGTObjectsContext> objectsContext)
{
    this->object = object;
    this->camera = camera;
    this->objectsContext = objectsContext;
}

void WeaponController::step()
{

    ticks += 0.23;

    auto mainCharacterPosition =  FSEGTUtils::getObjectPosition(object);

    auto mainCharacterGameplayProperties = DMUtils::getObjectGameplayProperties(object);
    auto weapon = mainCharacterGameplayProperties->weapon;

    if (weapon.get() == nullptr)
    {
        return;
    };

    auto isMoving = false;

    if (previousPosition.get() != nullptr)
    {
        isMoving = previousPosition->x != mainCharacterPosition->x || previousPosition->y != mainCharacterPosition->y || previousPosition->z != mainCharacterPosition->z;
    }
    if (isMoving)
    {
        if (amplitudeMultiplier < 0.014)
        {
            amplitudeMultiplier += 0.001;
        }
    }
    else if (amplitudeMultiplier > 0)
    {
        amplitudeMultiplier -= 0.001;
    }

    auto amplitude = sin(ticks) * amplitudeMultiplier;

    auto position = FSEGTUtils::getObjectPosition(camera)->copy();
    auto rotation = FSEGTUtils::getObjectRotation(camera);

    glm::mat4 matrix(1.0);

    matrix = glm::translate(matrix, glm::vec3(position->x, position->y, position->z));

    matrix = glm::rotate(matrix, rotation->x, glm::vec3(1.f, 0.f, 0.f));
    matrix = glm::rotate(matrix, rotation->y, glm::vec3(0.f, 1.f, 0.f));
    matrix = glm::rotate(matrix, rotation->z, glm::vec3(0.f, 0.f, 1.f));

    matrix = glm::translate(matrix, glm::vec3(0.2 + amplitude * 0.2, -0.2 + amplitude * 0.4, -0.3));

    auto result = matrix * glm::vec4(0.f, 0.f, 0.f, 1.f);

    position->x = result.x;
    position->y = result.y;
    position->z = result.z;

    FSEGTUtils::getObjectPosition(weapon)->populate(position);
    FSEGTUtils::getObjectRotation(weapon)->y = rotation->y + 4.84;

    objectsContext->updateObject(weapon);

    previousPosition = mainCharacterPosition->copy();
}