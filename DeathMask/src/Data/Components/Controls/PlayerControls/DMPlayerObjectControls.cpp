/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   DMPlayerControls.cpp
 * Author: demensdeum
 *
 * Created on August 20, 2017, 12:55 PM
 */

#include "DMPlayerObjectControls.h"
#include <iostream>

using namespace std;

#include <glm/gtc/matrix_transform.hpp>
#include <FlameSteelEngineGameToolkit/Utils/FSEGTUtils.h>

#define DMPLAYEROBJECTCONTROLSMOVEBYDIRECTION 1

void DMPlayerObjectControls::step(shared_ptr<DMObjectControlsDelegate> delegate)
{

    auto step = 0.006;
    auto frictionCap = step * 4;
    auto frictionStep = step * 0.4;
    auto frictionRounding = step * 0.2;

    if (frictionX < 0)
    {
        frictionX += frictionStep;
    }
    else if (frictionX > 0)
    {
        frictionX -= frictionStep;
    }

    if (frictionZ < 0)
    {
        frictionZ += frictionStep;
    }
    else if (frictionZ > 0)
    {
        frictionZ -= frictionStep;
    }

    if (frictionX < frictionRounding && frictionX > 0)
    {
        frictionX = 0;
    }
    else if (frictionX > -frictionRounding && frictionX < 0)
    {
        frictionX = 0;
    }

    if (frictionZ < frictionRounding && frictionZ > 0)
    {
        frictionZ = 0;
    }
    else if (frictionZ > -frictionRounding && frictionZ < 0)
    {
        frictionZ = 0;
    }

    bool updateNeeded = false;

    auto lockedObject = object.lock();

    if (lockedObject.get() == nullptr)
    {
        return;
    }

    auto rotation = FSEGTUtils::getObjectRotation(lockedObject);
    auto position = FSEGTUtils::getObjectPosition(lockedObject);

    bool leftKeyPressed = inputController->isLeftKeyPressed();
    bool rightKeyPressed = inputController->isRightKeyPressed();
    bool downKeyPressed = inputController->isDownKeyPressed();
    bool upKeyPressed = inputController->isUpKeyPressed();

    int stickOffset = 6;

    auto moveStick = stickController->moveStick;

    if (moveStick.get() != nullptr)
    {

        if (moveStick->x < -stickOffset)
        {
            leftKeyPressed = true;
        }

        if (moveStick->x > stickOffset)
        {
            rightKeyPressed = true;
        }

        if (moveStick->y < -stickOffset)
        {
            upKeyPressed = true;
        }

        if (moveStick->y > stickOffset)
        {
            downKeyPressed = true;
        }

    }

    auto viewStick = stickController->viewStick;

    if (viewStick.get() != nullptr)
    {

        if (viewTouchStartX == -1)
        {
            viewTouchStartX = rotation->y;
        }
        else
        {
            rotation->y = viewTouchStartX - float(viewStick->x) / 200;
        }

    }
    else
    {

        viewTouchStartX = -1;

    }

    if (leftKeyPressed && frictionX > -frictionCap)
    {
        frictionX -= step;
    }

    if (rightKeyPressed && frictionX < frictionCap)
    {
        frictionX += step;
    }

    if (downKeyPressed && frictionZ < frictionCap)
    {
        frictionZ += step;
    }

    if (upKeyPressed && frictionZ > -frictionCap)
    {
        frictionZ -= step;
    }

    moveByRotation(frictionX, 0, frictionZ, delegate);

    updateNeeded = true;

    if (inputController->pointerXdiff != 0)
    {
        rotation->y -= float(inputController->pointerXdiff) / 200;
    }

    if (updateNeeded)
    {
        delegate->objectsControlsDelegateObjectDidUpdate(lockedObject);
    }
}

void DMPlayerObjectControls::moveByRotation(float x, float y, float z, shared_ptr<DMObjectControlsDelegate> delegate)
{

    auto lockedObject = object.lock();

    if (lockedObject.get() == nullptr)
    {
        return;
    }

    auto blockDistance = 4;

    auto position = FSEGTUtils::getObjectPosition(lockedObject)->copy();
    auto rotation = FSEGTUtils::getObjectRotation(lockedObject);

    glm::mat4 matrix(1.0);

    matrix = glm::translate(matrix, glm::vec3(position->x, position->y, position->z));

    matrix = glm::rotate(matrix, rotation->x, glm::vec3(1.f, 0.f, 0.f));
    matrix = glm::rotate(matrix, rotation->y, glm::vec3(0.f, 1.f, 0.f));
    matrix = glm::rotate(matrix, rotation->z, glm::vec3(0.f, 0.f, 1.f));

    matrix = glm::translate(matrix, glm::vec3(x, y, z));

    auto checkPositionMatrix = glm::translate(matrix, glm::vec3(x * blockDistance, y * blockDistance, z * blockDistance));
    auto checkPositionVector = checkPositionMatrix * glm::vec4(0.f, 0.f, 0.f, 1.f);

    auto gameCheckPositionVector = make_shared<FSEGTVector>();
    gameCheckPositionVector->x = checkPositionVector.x;
    gameCheckPositionVector->y = checkPositionVector.y;
    gameCheckPositionVector->z = checkPositionVector.z;

    auto result = matrix * glm::vec4(0.f, 0.f, 0.f, 1.f); // to vector

    position->x = result.x;
    position->y = result.y;
    position->z = result.z;


    bool allDirectionsAllowed = checkForFreeRectangle(delegate, lockedObject, gameCheckPositionVector);
    if (allDirectionsAllowed == true)
    {
        FSEGTUtils::getObjectPosition(lockedObject)->populate(position);
        return;
    }

    auto gameCheckPositionVectorOnlyX = gameCheckPositionVector->copy();
    gameCheckPositionVectorOnlyX->z = position->z;
    bool onlyXAllowed = checkForFreeRectangle(delegate, lockedObject, gameCheckPositionVectorOnlyX);
    if (onlyXAllowed == true)
    {
        FSEGTUtils::getObjectPosition(lockedObject)->x = position->x;
        return;
    }

    auto gameCheckPositionVectorOnlyZ = gameCheckPositionVector->copy();
    gameCheckPositionVectorOnlyZ->x = position->x;
    bool onlyZAllowed = checkForFreeRectangle(delegate, lockedObject, gameCheckPositionVectorOnlyZ);
    if (onlyZAllowed == true)
    {
        FSEGTUtils::getObjectPosition(lockedObject)->z = position->z;
        return;
    }
}

bool DMPlayerObjectControls::checkForFreeRectangle(shared_ptr<DMObjectControlsDelegate> delegate,
        shared_ptr<Object> lockedObject,
        shared_ptr<FSEGTVector> vector)
{

    auto boxSize = 0.2;

    for (auto checkX = 0; checkX < 3; checkX++ )
    {
        for (auto checkZ = 0; checkZ < 3; checkZ++)
        {

            auto gameCheckPositionVectorCopy = vector->copy();
            gameCheckPositionVectorCopy->x -= boxSize;
            gameCheckPositionVectorCopy->x += boxSize * checkX;

            gameCheckPositionVectorCopy->z -= boxSize;
            gameCheckPositionVectorCopy->z += boxSize * checkZ;

            if (delegate->objectsControlsIsObjectCanMoveToPosition(shared_from_this(), lockedObject, gameCheckPositionVectorCopy) == false)
            {
                return false;
            }
        }
    }

    return true;
}

DMPlayerObjectControls::~DMPlayerObjectControls()
{
}

