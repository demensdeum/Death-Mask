#ifndef DEATHMASKCAMERACONTROLLER_H_
#define DEATHMASKCAMERACONTROLLER_H_

#include <FlameSteelEngineGameToolkit/Data/Components/Vector/FSEGTVector.h>
#include <FlameSteelEngineGameToolkit/Controllers/FSEGTObjectsContext.h>
#include <FlameSteelCore/Object.h>
#include <memory>

using namespace std;

namespace DeathMaskGame
{

class CameraController: public enable_shared_from_this<CameraController>
{

public:
    CameraController(shared_ptr<Object> object, shared_ptr<Object> camera, shared_ptr<FSEGTObjectsContext> objectsContext);
    void step();

private:
    shared_ptr<FSEGTVector> previousPosition;
    shared_ptr<Object> object;
    shared_ptr<Object> camera;
    shared_ptr<FSEGTObjectsContext> objectsContext;

    float ticks = 0;
    float amplitudeMultiplier = 0;
};

};

#endif