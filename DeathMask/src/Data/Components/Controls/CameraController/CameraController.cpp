#include <FlameSteelEngineGameToolkit/Utils/FSEGTUtils.h>
#include "CameraController.h"
#include <iostream>
#include <math.h>

using namespace DeathMaskGame;

CameraController::CameraController(shared_ptr<Object> object, shared_ptr<Object> camera, shared_ptr<FSEGTObjectsContext> objectsContext)
{
    this->object = object;
    this->camera = camera;
    this->objectsContext = objectsContext;
}

void CameraController::step()
{

    ticks += 0.23;

    auto mainCharacterPosition =  FSEGTUtils::getObjectPosition(object);

    auto isMoving = false;

    if (previousPosition.get() != nullptr)
    {
        isMoving = previousPosition->x != mainCharacterPosition->x || previousPosition->y != mainCharacterPosition->y || previousPosition->z != mainCharacterPosition->z;
    }
    if (isMoving)
    {
        if (amplitudeMultiplier < 0.014)
        {
            amplitudeMultiplier += 0.001;
        }
    }
    else if (amplitudeMultiplier > 0)
    {
        amplitudeMultiplier -= 0.001;
    }

    auto amplitude = sin(ticks) * amplitudeMultiplier;

    auto cameraPosition = FSEGTUtils::getObjectPosition(camera);
    cameraPosition->x = mainCharacterPosition->x;
    cameraPosition->y = mainCharacterPosition->y + amplitude;
    cameraPosition->z = mainCharacterPosition->z;

    auto mainCharacterRotation = FSEGTUtils::getObjectRotation(object);
    auto cameraRotation = FSEGTUtils::getObjectRotation(camera);
    cameraRotation->x = mainCharacterRotation->x;
    cameraRotation->y = mainCharacterRotation->y;
    cameraRotation->z = mainCharacterRotation->z;

    objectsContext->updateObject(camera);

    previousPosition = mainCharacterPosition->copy();
}